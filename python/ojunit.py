import unittest, logging, os, sys

from engineconfig import getConfig

class OJTestCase(unittest.TestCase):

    def setUp(self):

        logger = logging.getLogger('main')
        hdlr = logging.StreamHandler()
        formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s')
        hdlr.setFormatter(formatter)
        logger.addHandler(hdlr)
        logger.setLevel(logging.DEBUG)

        execfile(os.path.join('..', 'testdata', 'test_config.py'))
        self.config = getConfig()
        self.ds = self.config.datasources[0]
        self.dbname = os.path.join('..', 'testdata', self.config.testdb)

        self.logger = logger
