#!/usr/bin/env python2

import string
from string import split
from os import path
from Queue import Queue
from subprocess import Popen, PIPE

class EngineConfig:

    def __init__(self):
        self.profile = 'default'
        self.test_threads = 4
        self.retry = 10
        self.retry_wait = 0.5
        self.datasources = []
        self.fetch_interval = 2.5
        self.mips = 1000
        self.languages = {}
        self.maxmem = 65536
        self.maxtime = 300
        self.compile_timelimit = 30
        self.output_size_limit = 1024 * 1024 * 2
        self.output_sendback_size_limit = 1024 * 64
        self.logdir = 'log'
        self.debug = False
        self.judgescript_wait = 10
        self.python = '/usr/bin/python2'
        self.no_cleanup = False
        self.datadir = path.join('data', self.profile)
        self.rundir_root = '/var/lib/bitoj'

        self.runas = Queue()
        self.setup_testers()

        # Init runas with ojrun?? users
        pa = Popen(['getent', 'passwd'], stdout=PIPE)
        pb = Popen(['awk', '-F:', '/^ojrun[0-9][0-9]:/{print $1}'],
                   stdin=pa.stdout, stdout=PIPE)
        output = pb.communicate()[0]
        if output:
            for user in string.split(output, '\n'):
                user = string.strip(user)
                if user: self.runas.put(user)
        pa.wait()
        pb.wait()

    def add_tester(self, profile, tester):
        self.languages[profile] = tester

    def get_tester(self, profile):
        if self.languages.has_key(profile):
            return self.languages[profile]

    def add_datasource(self, ds):
        from datasource import DataSource
        self.datasources.append(DataSource(ds))

    def add_xmlrpc_datasource(self, url):
        from datasource import XmlRpcDataSource
        self.add_datasource(XmlRpcDataSource(url))

    def setup_testers(self):

        default_compileguard = (
            '<judgehome>/scripts/compile-guard', ' <datadir>'
        )
        default_runguard = split(
            '/usr/bin/sudo -u <user> <judgehome>/scripts/binary-guard ' + 
            '-e <extraproc> ' +
            '-t <timelimit> -T 5 -m <maxmem> -d <rundir> -o <statfile> -p -x',
            ' '
        )
        maxmem_runguard = split(
            '/usr/bin/sudo -u <user> <judgehome>/scripts/binary-guard ' + 
            '-e <extraproc> ' +
            '-t <timelimit> -T 5 -m <maxmem> -d <rundir> -o <statfile> -p -x',
            ' '
        )
        scilab_runguard = split(
            '/usr/bin/sudo -u <user> ' +
            '<judgehome>/scripts/scilab-guard -t <timelimit> -T 5 -m 0 ' +
            '-e <extraproc> ' +
            '-d <rundir> -o <statfile> -p -x', ' '
        )
        java_runguard = split(
            '/usr/bin/sudo -u <user> ' +
            '<judgehome>/scripts/java-guard -t <timelimit> -T 5 -m 0 ' +
            '-e <extraproc> ' +
            '-d <rundir> -o <statfile> -p -x', ' '
        )
        python_runguard = split(
            '/usr/bin/sudo -u <user> <judgehome>/scripts/python-guard ' +
            '-t <timelimit> -T 5 -m <maxmem> -d <rundir> -o <statfile> -p -x',
            ' '
        )
        mono_runguard = split(
            '/usr/bin/sudo -u <user> ' +
            '<judgehome>/scripts/mono-guard -t <timelimit> -T 10 -m <maxmem> ' +
            '-e <extraproc> ' +
            '-d <rundir> -o <statfile> -p -x', ' '
        )
        bash_runguard = split(
            '/usr/bin/sudo -u <user> <judgehome>/scripts/bash-guard ' +
            '-e <extraproc> ' +
            '-t <timelimit> -T 5 -m <maxmem> -d <rundir> -o <statfile> -p -x',
            ' '
        )
        default_compare = (
            '<judgehome>/scripts/compare-guard', '<language>', '<codefile>',
            '<stdinfile>', '<stdoutfile>', '<resultfile>'
        )

        # Options for testers
        from tester import SimpleTester, ComboTester
        cbc = SimpleTester(source = 'main.c',
            target = 'main',
            compile = ('<judgehome>/scripts/gcc-3.3-bc',),
            run = ('<datadir>/main', ),
            runenv = {
                'GCC_BOUNDS_OPTS' : '-no-message -no-statistics -no-check-mmap',
            },
            basemem = {'Data' : 48, 'Stack' : 84 },
            compileguard = default_compileguard,
            runguard = maxmem_runguard,
        )

        cnobc = SimpleTester(
            source = 'main.c', target = 'main',
            compile = ('<judgehome>/scripts/gcc-3.3-nobc',),
            run = ('<datadir>/main', ),
            runenv = {},
            basemem = {'Data' : 28, 'Stack' : 84 },
            compileguard = default_compileguard,
            runguard = default_runguard,
        )
        
        gcc33 = ComboTester()
        gcc33.add_tester(cbc, has_timelimit = False, has_memlimit = False,
                         check_result = True, is_lasttest = False)
        gcc33.add_tester(cnobc, has_timelimit = True, has_memlimit = True,
                         check_result = True, is_lasttest = True)
        gcc33.comparecmd = default_compare
        gcc33.source = 'main.c'

        gcc = SimpleTester(
            source = 'main.c', target = 'main',
            compile = ('<judgehome>/scripts/gcc',),
            run = ('<datadir>/main', ),
            runenv = {},
            basemem = {'Data' : 28, 'Stack' : 84 },
            compileguard = default_compileguard,
            runguard = default_runguard,
        )
        
        gxx = SimpleTester(
            source = 'main.cpp', target = 'main',
            compile = ('<judgehome>/scripts/g++',),
            run = ('<datadir>/main',),
            runenv = {},
            basemem = {'Data' : 52, 'Stack' : 84 },
            compileguard = default_compileguard,
            runguard = default_runguard,
            comparecmd = default_compare,
        )

        scilab = SimpleTester(
            source = 'main.sci', target = 'main.sci',
            compile = ('<judgehome>/scripts/scilab-add-exit',),
            run = split('/usr/bin/scilab -nb -nwni -f <datadir>/main.sci'),
            runenv = {},
            basemem = {'RSS' : 7560 },
            baseproc = 8,
            compileguard = default_compileguard,
            runguard = scilab_runguard,
            comparecmd = default_compare,
        )
        
        java = SimpleTester(
            source = 'Main.java', target = 'Main.class',
            compile = ('<judgehome>/scripts/javac',),
            run = split('/usr/bin/java -cp <datadir> -Xms8M -Xmx256M Main'),
            runenv = {},
            basemem = {'RSS' : 7560 },
            baseproc = 13,
            compileguard = default_compileguard,
            runguard = java_runguard,
            comparecmd = default_compare,
        )

        fpc = SimpleTester(
            source = 'main.pas', target = 'main',
            compile = ('<judgehome>/scripts/fpc',),
            run = ('<datadir>/main', ),
            runenv = {},
            basemem = {'Data' : 32, 'Stack' : 84 },
            compileguard = default_compileguard,
            runguard = default_runguard,
            comparecmd = default_compare,
        )

        python = SimpleTester(
            source = 'main.py', target = 'main.py',
            compile = ('/bin/true',),
            run = ('/usr/bin/python', '<datadir>/main.py'),
            runenv = {},
            basemem = {'RSS' : 2048 },
            compileguard = (),
            runguard = python_runguard,
            comparecmd = default_compare,
        )

        dmcs = SimpleTester(
            source = 'main.cs', target = 'main.exe',
            compile = ('<judgehome>/scripts/dmcs',),
            run = ('/usr/bin/mono', '<datadir>/main.exe'),
            runenv = { 'MONO_SHARED_DIR' : '<datadir>' },
            basemem = {'RSS' : 8192 },
            baseproc = 3,
            compileguard = default_compileguard,
            runguard = mono_runguard,
            comparecmd = default_compare,
        )

        bash = SimpleTester(
            source = 'main.sh', target = 'main.sh',
            compile = ('/bin/true',),
            run = ('/bin/bash', '<datadir>/main.sh'),
            runenv = {},
            basemem = {'RSS' : 2048 },
            extraproc = 3,
            compileguard = (),
            runguard = bash_runguard,
            comparecmd = default_compare,
        )
        
        #self.add_tester('gcc-3.3', gcc33)
        #self.add_tester('gcc-3.3-nobc', cnobc)
        #self.add_tester('gcc-3.3-bc', cbc)

        self.add_tester('gcc', gcc)
        self.add_tester('g++', gxx)
        self.add_tester('java', java)
        self.add_tester('fpc', fpc)
        self.add_tester('scilab', scilab)
        self.add_tester('python', python)
        self.add_tester('dmcs', dmcs)
        self.add_tester('bash', bash)

config = None

def getConfig():
    global config
    if not config:
        config = EngineConfig()
    return config

# vim: set expandtab tabstop=4 shiftwidth=4:
