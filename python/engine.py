
import logging, os, thread, threading, time, Queue

from engineconfig import getConfig

class JudgeEngine:

    def __init__(self):
        self.quit_event = threading.Event()
        self.test_queue = Queue.Queue()

        self.config = getConfig()
        self.quit_event.clear()
        self.logger = logging.getLogger('main')

    def run(self):
        # one thread mode is good for debugging
        if self.config.test_threads == 1:
            thread.start_new_thread(self.transport, ())
            self.test()
        else:
            for i in range(self.config.test_threads):
                thread.start_new_thread(self.test, ())
            self.transport()

        for ds in self.config.datasources:
            ds.reset_submits()
        self.logger.info('main thread quit')
        return

    def transport(self):
        total = 0
        while not self.quit_event.isSet():
            self.logger.info(
                "%d submit in test queue, %d processed" % \
                (self.test_queue.qsize(), total))
            c = 16 - self.test_queue.qsize()
            if c > 0:
                langs = ','.join(self.config.languages.keys())
                submits = self.config.datasources[0].get_submits(c, langs)
                for submit in submits:
                    self.logger.info('Add submit %s to queue' % submit.id)
                    self.test_queue.put(submit)
                total = total + len(submits)
            time.sleep(self.config.fetch_interval)
        self.logger.info("fetch thread quit")
        return

    def test(self):
        while not self.quit_event.isSet():
            try:
                submit = self.test_queue.get(True, self.config.fetch_interval)
            except Queue.Empty:
                continue
            tester = self.config.get_tester(submit.language)
            if tester:
                tester.test(submit)
            else:
                submit.set_status('compile_failed')
                submit.set_compilemessage('No compiler specified')
        self.logger.info("test thread quit")
        return
    
    def quit(self, signum, frame):
        self.logger.info('signal receive: %d' % signum)
        self.quit_event.set()
        return

class JudgeError(Exception): pass

# vim: set expandtab tabstop=4 shiftwidth=4:
