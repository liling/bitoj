#!/usr/bin/env python

import sys, string, hashlib, os
from stat import *

def check_file(name, size, md5sum):
    global result

    m = hashlib.md5()
    if os.path.isfile(name):
        s = os.stat(name)[ST_SIZE]
        if s == string.atoi(size):
            f = open(name, 'rb')
            m.update(f.read(string.atoi(size)))
            f.close()
            d = m.hexdigest()
            result.write("%s %d %s\n" % (name, s, d))
            return d == md5sum
        else:
            result.write("Size of %s is %d\n" % (name, s))
    else:
        result.write("%s not found\n" % name)
    return False

if __name__ == '__main__':
    result = open(sys.argv[3], 'w+')
    fstdout = open(sys.argv[2], 'r')
    for line in fstdout:
        name, size, md5sum = string.split(string.strip(line), ' ')
        if not check_file(name, size, md5sum):
            print 'WA'; break
    fstdout.close()
    result.close()
    print 'AC'
