DROP TABLE language;
DROP TABLE problem;
DROP TABLE problem_testcase;
DROP TABLE submit;
DROP TABLE submit_test_result;

CREATE TABLE language (
    id INTEGER NOT NULL PRIMARY KEY,
    profile TEXT NOT NULL UNIQUE,
    description TEXT
);

INSERT INTO language VALUES (1, 'gcc-3.3', 'GCC 3.3');
INSERT INTO language VALUES (2, 'g++-3.3', 'G++ 3.3');
INSERT INTO language VALUES (3, 'java-1.5', 'Sun Java 1.5');
INSERT INTO language VALUES (4, 'java-1.6', 'Sun Java 1.6');
INSERT INTO language VALUES (5, 'fpc-2.0', 'Free Pascal 2.0');
INSERT INTO language VALUES (6, 'python-2.5', 'Free Pascal 2.0');

CREATE TABLE problem (
    id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    timemodified INTEGER NOT NULL,
    validator TEXT,
    validator_type TEXT
);
CREATE TABLE problem_testcase (
    id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    pid INTEGER NOT NULL,
    timemodified INTEGER NOT NULL,
    input TEXT,
    output TEXT,
    timelimit INTEGER,
    memlimit INTEGER
);

-- A + B problem
INSERT INTO problem VALUES (1, 0, NULL, NULL);
INSERT INTO problem_testcase VALUES (101, 1, 0, '1 2', '3', 10, 1024*1024);
INSERT INTO problem_testcase VALUES (102, 1, 0, '2 2', '4', 1, 1024*1024);

-- Problem with judge script
INSERT INTO problem VALUES (2, 0, '
import string, sys
f = file(sys.argv[2], ''r'')
out = string.join(f.readlines(), ''\n'')
f.close()
f = file(sys.argv[3], ''r'')
rst = string.join(f.readlines(), ''\n'')
f.close()
if out == rst: print ''AC''
else: print ''WA''
', 'python');
INSERT INTO problem_testcase VALUES (201, 2, 0, '3', '3', 1, 1024*1024);

-- Problem that test security
INSERT INTO problem VALUES (3, 0, NULL, NULL);
INSERT INTO problem_testcase VALUES (301, 3, 0, '/etc/passwd r', 'FAIL', 1, 1024*1024);
INSERT INTO problem_testcase VALUES (302, 3, 0, '/tmp/write w', 'FAIL', 1, 1024*1024);

-- Other security test
INSERT INTO problem VALUES (4, 0, NULL, NULL);
INSERT INTO problem_testcase VALUES (401, 4, 0, '', '', 1, 1024*1024);

CREATE TABLE submit (
    id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    pid INTEGER NOT NULL,
    lang INTEGER NOT NULL,
    code TEXT,
    compilemessage TEXT,
    status INTEGER
);

-- No Source
INSERT INTO submit VALUES (1, 1, 1, '', NULL, 0);

-- 
INSERT INTO submit VALUES (2, 1, 1, '#include <stdio.h>', NULL, 0);

-- Step, AC, TargetDeleted
INSERT INTO submit VALUES (3, 1, 1, '#include <stdio.h>
int main() {
  int a, b, c;
  scanf("%d %d", &a, &b);
  c = a + b;
  printf("%d\n", c);
  return 0;
}', NULL, 0);

-- WA
INSERT INTO submit VALUES (4, 1, 1, '#include <stdio.h>
int main() {
  printf("3\n");
  return 0;
}', NULL, 0);

-- Zero
INSERT INTO submit VALUES (5, 1, 1, '#include <stdio.h>
int main() {
  putchar(''3'');
  putchar(''\0'');
  putchar(''\n'');
  return 0;
}', NULL, 0);

-- Binary
INSERT INTO submit VALUES (6, 1, 1, '#include <stdio.h>
int main() {
  putchar(0xBB);
  return 0;
}', NULL, 0);

-- CE
INSERT INTO submit VALUES (7, 1, 1, 'int main() { qwriyuiqhruiarhwqrqwe; }', NULL, 0);

INSERT INTO submit VALUES (8, 2, 1, '#include <stdio.h>
int main() { printf("3\n"); }', NULL, 0);
INSERT INTO submit VALUES (9, 2, 1, '#include <stdio.h>
int main() { printf("4\n"); }', NULL, 0);

-- OLE
INSERT INTO submit VALUES (10, 1, 1, '#include <stdio.h>
int main() {
  int i;
  char buf[1025];
  memset(buf, ''X'', 1024); buf[1024] = 0;
  for (i = 0; i < 20480; i++) puts(buf);
  return 0;
}', NULL, 0);

-- TLE
INSERT INTO submit VALUES (11, 1, 1, 'int main() { for (;;); }', NULL, 0);

-- C
INSERT INTO submit VALUES (1001, 1, 1, '
#include <stdio.h>

int main()
{
    int a, b;
    scanf("%d %d", &a, &b);
    printf("%d\n", a + b);
    return 0;
}
', NULL , 0);

-- C++
INSERT INTO submit VALUES (1002, 1, 2, '
#include <iostream>

int main()
{
    int a, b;
    std::cin >> a >> b;
    std::cout << a + b << std::endl;
    return 0;
}
', NULL , 0);

-- Java
INSERT INTO submit VALUES (1004, 1, 4, '
import java.util.Scanner;

public class Main {
    public static void main(String argv[]) {
        Scanner scan = new Scanner(System.in);
        int a = scan.nextInt();
        int b = scan.nextInt();
        System.out.println(a + b);
    }
}
', NULL , 0);

-- Pascal
INSERT INTO submit VALUES (1005, 1, 5, '
program p1000(Input,Output);

var
  a,b:Integer;
begin
   Readln(a,b);
   Writeln(a+b);
end.
', NULL, 0);

-- Python
INSERT INTO submit VALUES (1006, 1, 6, '
import sys, string

line = sys.stdin.readline()
a, b = line.split(" ")
a = string.atoi(a)
b = string.atoi(b)
print a + b
', NULL, 0);

-- Security
-- C file permission
INSERT INTO submit VALUES (2311, 3, 1, '
#include <stdio.h>
#include <unistd.h>

int main() {
    char filename[20], mode[8];
    FILE *fp;

    scanf("%s", filename);
    scanf("%s", mode);
    fp = fopen(filename, mode);
    if (fp == NULL) {
        printf("FAIL");
        exit(0);
    }
    fclose(fp);
    printf("SUCCESS");

    return 0;
}
', NULL, 0);

-- Java file permission
INSERT INTO submit VALUES (2331, 3, 3, '
import java.io.*;
import java.util.*;

public class Main {

    public static void main(String argv[]) {

        Scanner scan = new Scanner(System.in);
        String line = scan.nextLine();
        int pos = line.indexOf('' '');
        String filename = line.substring(0, pos);
        char mod = line.charAt(pos + 1);

        try {
            if (mod == ''r'') {
                Scanner read = new Scanner(new File(filename));
                System.out.println(read.nextLine());
            } else if (mod == ''w'') {
                FileWriter writer = new FileWriter(filename);
                writer.write("hello\n");
                writer.close();
            }
            System.out.println("SUCCESS");
        } catch (Exception e) {
            System.out.println("FAIL");
        }
    }

}
', NULL, 0);

-- Python file permission
INSERT INTO submit VALUES (2361, 3, 6, '
import sys, string
line = sys.stdin.readline().strip()
fn, m = line.split('' '')
try:
    f = file(fn, m)
    if m == "r":
        print f.readline()
    else:
        f.write("hello")
    f.close()
    print "SUCCESS"
except:
    print "FAIL"
', NULL, 0);

-- sleep
INSERT INTO submit VALUES (2411, 4, 1, '
#include <unistd.h>
int main() {
    sleep(10);
    return 0;
}
', NULL, 0);

-- fork
INSERT INTO submit VALUES (2412, 4, 1, '
#include <stdio.h>
#include <unistd.h>

int main() {
    int pid;

    pid = fork();
    if (pid == 0) {
        for(;;) {
            printf("in fork\n");
            sleep(1);
        }
        return 0;
    }
    sleep(100);

    return 0;
}
', NULL, 0);

-- daemon
INSERT INTO submit VALUES (2413, 4, 1, '
#include <stdio.h>
#include <unistd.h>

int main() {
    int pid;

    daemon(0, 0);
    for(;;) {
        printf("in fork\n");
        sleep(1);
    }
    return 0;
}
', NULL, 0);


CREATE TABLE submit_test_result (
    id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    sid INTEGER NOT NULL,
    tid INTEGER NOT NULL,
    stdout TEXT,
    stderr TEXT,
    exitcode INTEGER,
    signal INTEGER,
    timeused REAL,
    memused INTEGER
);

