from engineconfig import getConfig
from datasource import XmlRpcDataSource

from os import path

# Options for XMLRPC datasource
config = getConfig()
config.no_cleanup = True
config.add_datasource(XmlRpcDataSource('http://127.0.0.1:8000/'))

# Options for tester
config.datadir = path.abspath('../testdata/data')

# This is for datasource testcases
config.testdb = 'basic.db'
config.judgehome = path.dirname(path.dirname(path.abspath(sys.argv[0])))
