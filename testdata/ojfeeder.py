#!/usr/bin/python

from SimpleXMLRPCServer import SimpleXMLRPCServer
import sys, xmlrpclib, os

class MyServer(SimpleXMLRPCServer):

    def serve_forever(self):
        self.quit = 0
        while not self.quit:
            self.handle_request()

def kill():
    global server
    server.quit = 1
    return 1
    
# Create server
server = MyServer(('127.0.0.1', 8000))
server.register_multicall_functions()
server.register_introspection_functions()
server.register_function(kill)

def get_conn():
    # Connect to database
    from pysqlite2 import dbapi2 as sqlite
    return sqlite.connect('basic.db')

def get_judge_id():
    return 1

def reset_submits(judgeid):
    return 0

def get_submits(judgeid, limit):
    conn = get_conn()
    cur = conn.cursor()

    languages = {}
    cur.execute("SELECT id, profile FROM language")
    for row in cur:
        languages[row[0]] = row[1]

    ret = []
    cur.execute('SELECT id, pid, lang, code FROM submit')
    for row in cur:
        submit = {}
        submit['id'] = '%010d' % row[0]
        submit['problem_id'] = '%010d' % row[1]
        try:
            submit['language'] = languages[row[2]]
        except IndexError:
            continue
        submit['code'] = row[3]
        ret.append(submit)

    cur.close()
    conn.close()
    return ret

def update_submit_compilemessage(id, msg):
    msg = msg.__str__()
    conn = get_conn()
    cur = conn.cursor()
    cur.execute("UPDATE submit SET compilemessage=? WHERE id=?", (msg, id))
    conn.commit()
    cur.close()
    conn.close()
    return (id, msg)

def get_submit_compilemessage(id):
    conn = get_conn()
    cur = conn.cursor()
    cur.execute("SELECT compilemessage FROM submit WHERE id=?", (id,))
    msg = cur.fetchone()
    cur.close()
    conn.close()
    if msg[0]:
        return msg[0]
    else:
        return ''

def update_submit_status(id, status):
    s = 0
    if status == 'compiling':
        s = 2
    elif status == 'compile_success':
        s = 3
    elif status == 'compile_fail':
        s = 11
    elif status == 'running':
        s = 4
    elif status == 'finish':
        s = 10
    conn = get_conn()
    cur = conn.cursor()
    cur.execute("UPDATE submit SET status=? WHERE id=?", (s, id))
    conn.commit()
    cur.close()
    conn.close()
    return (id, status)

def get_submit(id):
    conn = get_conn()
    cur = conn.cursor()

    languages = {}
    cur.execute("SELECT id, profile FROM language")
    for row in cur:
        languages[row[0]] = row[1]

    cur.execute('SELECT id, pid, lang, code FROM submit where id=?', (id,))
    row = cur.fetchone()
    if row:
        submit = {
            'id' : '%010d' % row[0], 'problem_id' : '%010d' % row[1],
            'code' : row[3]
        }
        try:
            submit['language'] = languages[row[2]]
        except IndexError:
            pass
    else:
        submit = None
    return submit

def get_submit_status(id):
    conn = get_conn()
    cur = conn.cursor()
    cur.execute("SELECT status FROM submit WHERE id=?", (id,))
    msg = cur.fetchone()
    cur.close()
    conn.close()
    s = { 0 : 'new',
          2 : 'compiling', 3 : 'compile_success', 4 : 'running',
          10 : 'finish', 11 : 'compile_failed' }
    return s[msg[0]]

def get_problem(id):
    conn = get_conn()
    cur = conn.cursor()
    cur.execute("SELECT id, timemodified, validator, validator_type FROM problem WHERE id=?", (id,))
    p = cur.fetchone()
    cur.close()
    conn.close()
    ret = {
        'id' : '%010d' % p[0], 'timemodified' : p[1],
        'validator_code' : p[2], 'validator_type' : p[3],
        'generator_code' : '', 'generator_type' : '',
        'standard_code' : ''
    }
    if not ret['validator_code']: ret['validator_code'] = ''
    if not ret['validator_type']: ret['validator_type'] = ''
    return ret

def get_tests(id, full):
    conn = get_conn()
    cur = conn.cursor()

    ret = []
    if full:
        sql = 'SELECT id, pid, timemodified, timelimit, memlimit, input, output FROM problem_testcase WHERE pid=' + id
    else:
        sql = 'SELECT id, pid, timemodified, timelimit, memlimit FROM problem_testcase WHERE pid=' + id
    print >> sys.stderr, sql
    cur.execute(sql)
    for row in cur:
        t = {}
        t['id'] = '%010d' % row[0]
        t['problem_id'] = '%010d' % row[1]
        t['timemodified'] = row[2]
        t['timelimit'] = row[3]
        t['memlimit'] = row[4]
        if full:
            t['input'] = row[5]
            t['output'] = row[6]
        else:
            t['input'] = ''
            t['output'] = ''
        ret.append(t)

    cur.close()
    conn.close()
    return ret

def get_test(id):
    ret = {}

    conn = get_conn()
    cur = conn.cursor()

    sql = 'SELECT id, pid, timemodified, timelimit, memlimit, input, output FROM problem_testcase WHERE id=' + id
    print >> sys.stderr, sql
    cur.execute(sql)
    row = cur.fetchone()
    ret['id'] = '%010d' % row[0]
    ret['problem_id'] = '%010d' % row[1]
    ret['timemodified'] = row[2]
    ret['timelimit'] = row[3]
    ret['memlimit'] = row[4]
    ret['input'] = row[5]
    ret['output'] = row[6]

    cur.close()
    conn.close()

    return ret

def update_submit_test_results(id, results):
    print >>sys.stderr, results
    conn = get_conn()
    cur = conn.cursor()

    count = 0
    for result in results:
        r = []
        r.append(id)
        r.append(result['test_id'])
        r.append(result['stdout'].__str__())
        r.append(result['stderr'].__str__())
        r.append(result['exitcode'])
        r.append(result['signal'])
        r.append(result['timeused'])
        r.append(result['memused'])
        cur.execute("INSERT INTO submit_test_result (sid, tid, stdout, stderr, exitcode, signal, timeused, memused) VALUES (?, ?, ?, ?, ?, ?, ?, ?)", r)
        conn.commit()
        count += 1

    cur.close()
    conn.close()
    return count

def hello():
    return 'world'

server.register_function(hello, 'hello')
server.register_function(get_judge_id, 'oj.get_judge_id')
server.register_function(reset_submits, 'oj.reset_submits')
server.register_function(get_submits, 'oj.get_submits')
server.register_function(update_submit_compilemessage, 'oj.update_submit_compilemessage')
server.register_function(get_submit_compilemessage, 'oj.get_submit_compilemessage')
server.register_function(update_submit_status, 'oj.update_submit_status')
server.register_function(get_submit, 'oj.get_submit')
server.register_function(get_submit_status, 'oj.get_submit_status')
server.register_function(get_problem, 'oj.get_problem')
server.register_function(get_tests, 'oj.get_tests')
server.register_function(get_test, 'oj.get_test')
server.register_function(update_submit_test_results, 'oj.update_submit_test_results')

server.serve_forever()
print 'Closed'
