install:
	install -m 755 judge judge-daemon $(DESTDIR)/usr/lib/bitoj
	install -m 644 python/* $(DESTDIR)/usr/lib/bitoj/python
	install -m 755 scripts/* $(DESTDIR)/usr/lib/bitoj/scripts
	install -m 755 utils/* $(DESTDIR)/usr/lib/bitoj/utils
	install -m 644 apparmor-profiles/abstractions_bitoj \
		$(DESTDIR)/etc/apparmor.d/abstractions/bitoj
	install -m 644 apparmor-profiles/bitoj-guards $(DESTDIR)/etc/apparmor.d
	sed -i "s/\/usr\/local/\/usr\/lib/" $(DESTDIR)/etc/apparmor.d/bitoj-guards
	sed -i "s/\/usr\/local/\/usr\/lib/" $(DESTDIR)/etc/apparmor.d/abstractions/bitoj
	install -m 644 doc/sample-conf.py $(DESTDIR)/etc/bitoj/conf-demo.py
	ln -s /etc/bitoj $(DESTDIR)/usr/lib/bitoj/conf
	ln -s /var/log/bitoj $(DESTDIR)/usr/lib/bitoj/log
	ln -s /var/lib/bitoj/data $(DESTDIR)/usr/lib/bitoj/data

clean:
	rm -f log/*
	rm -f python/*.pyc
	rm -rf bitoj*

deb:
	svn export . bitoj-1.1.1
	tar -zcf bitoj_1.1.1.orig.tar.gz bitoj-1.1.1
	cd bitoj-1.1.1 ; debuild ; cd .
	rm -rf bitoj-1.1.1
